package com.aarna.chat;

import java.util.Date;
import java.util.UUID;

import com.aarna.chat.nlp.NormalizedText;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ChatRequest {

	private final String id;
	private final String message;
	private final Date timestamp;
	
	private transient String intent;
	private transient NormalizedText text;
	private final transient ChatSession chatSession;
	
	public ChatRequest(final String message, final ChatSession chatSession) {
		this(UUID.randomUUID().toString(), message, new Date(), chatSession);
	}
	
}
