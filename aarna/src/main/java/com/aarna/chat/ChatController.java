package com.aarna.chat;

import static com.aarna.AarnaConstants.CHAT;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttribute;

@RestController
@RequestMapping("/api/v1/chat")
public class ChatController {
	
	@Autowired private ChatService conversationService;

	@GetMapping
	public ChatResponse converse(
			@RequestParam("q") final String query, 
			@SessionAttribute(CHAT) ChatSession chatSession) throws Exception {
		final ChatResponse chatResponse = new ChatResponse();
		final ChatRequest chatRequest = new ChatRequest(query, chatSession);
		conversationService.service(chatRequest, chatResponse);
		chatSession.getRequests().put(chatRequest.getId(), chatRequest);
		chatSession.getResponses().put(chatRequest.getId(), chatResponse);
		return chatResponse;
	}
	
}
