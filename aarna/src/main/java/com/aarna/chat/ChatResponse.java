package com.aarna.chat;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.aarna.chat.responder.RecommendationItem;

import lombok.Data;

@Data
public class ChatResponse {

	private String message = "";
	private Date timestamp = new Date();
	private final List<RecommendationItem> recommendations = new LinkedList<>();
	
}
