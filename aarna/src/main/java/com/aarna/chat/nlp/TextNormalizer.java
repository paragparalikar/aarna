package com.aarna.chat.nlp;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.amazonaws.util.IOUtils;

@Component
public class TextNormalizer implements Function<String, NormalizedText> {

	@Autowired
	private WordSpellChecker spellChecker;
	
	private Set<String> commonWords = new HashSet<>();
	
	@Value("${aarna.nlp.normalizer.common-words-path:classpath:nlp/common-words.csv}")
	private Resource commonWordsFilePath;
	
	@PostConstruct
	public void init() throws IOException {
		final InputStream inputStream = commonWordsFilePath.getInputStream();
		final String[] words = IOUtils.toString(inputStream).split(",");
		commonWords.addAll(Arrays.asList(words));
	}
	
	@Override
	public NormalizedText apply(String text) {
		if(!StringUtils.hasText(text)) return NormalizedText.EMPTY;
		final List<String> tokens = apply(Arrays.asList(text.split("[ ,;\\.]+")));
		return NormalizedText.builder()
				.tokens(tokens)
				.originalText(text)
				.normalizedText(tokens.stream().sorted().collect(Collectors.joining(" ")))
				.build();
	}
	
	public List<String> apply(List<String> words){
		return words.stream()
				.map(String::trim)
				.map(String::toLowerCase)
				.map(spellChecker::correct)
				.distinct()
				.filter(word -> !commonWords.contains(word))
				.collect(Collectors.toList());
	}

}
