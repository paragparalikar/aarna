package com.aarna.chat.nlp.similarity;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.text.similarity.SimilarityScore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class FuzzyMatchCountSimilarity implements SimilarityScore<Double> {
	
	@Value("${aarna.nlp.recommender.similarity.min:0.8}")
	private Double minSimilarity = 0.8;
	
	@Autowired
	private EditDistanceSimilarity delegate;

	@Override
	public Double apply(CharSequence left, CharSequence right) {
		final Double result = delegate.apply(left, right);
		log.debug("Computed similarity {} between input {} and {}", result, left, right);
		return result;
	}
	
	public Double apply(List<String> left, List<String> right) {
		final Double result = left.stream()
			.map(word -> apply(word, right))
			.collect(Collectors.summingDouble(Double::doubleValue)) / Double.valueOf(left.size());
		log.debug("Computed similarity {} between input {} and {}", result, left, right);
		return result;
	}
	
	private Double apply(String word, List<String> right) {
		final Double result = (double) right.stream()
				.filter(tag -> matches(word, tag))
				.count();
		log.debug("Computed similarity {} between input {} and {}", result, word, right);
		return result;
	}
	
	private boolean matches(String left, String right) {
		return left.charAt(0) == right.charAt(0) && 
				minSimilarity <= apply(left, right);
	}
	
	
}
