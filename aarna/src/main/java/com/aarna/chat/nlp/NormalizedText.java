package com.aarna.chat.nlp;

import java.util.Collections;
import java.util.List;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Value
@Builder
@EqualsAndHashCode(of = "normalizedText")
public final class NormalizedText {

	public static final NormalizedText EMPTY = NormalizedText.builder().build();
	
	@Builder.Default @NonNull private final String originalText = "";
	@Builder.Default @NonNull private final String normalizedText = "";
	@Builder.Default @NonNull private final List<String> tokens = Collections.emptyList();

	public List<String> getTokens(){
		return Collections.unmodifiableList(tokens);
	}
	
}
