package com.aarna.chat.nlp;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.swabunga.spell.engine.SpellDictionaryHashMap;
import com.swabunga.spell.engine.Word;
import com.swabunga.spell.event.SpellChecker;

@Component
public class WordSpellChecker {

	@Value("${aarna.nlp.spell.dictionary.directory:classpath:nlp/english_dic}")
	private Resource directory;
	
	private final SpellChecker spellChecker = new SpellChecker();
	
	@PostConstruct
	public void init() throws IOException {
		final SpellDictionaryHashMap dictionary = new SpellDictionaryHashMap();
		spellChecker.addDictionary(dictionary);
		final ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();
		final Resource[] resources = resourcePatternResolver.getResources("classpath:/nlp/english_dic/*.dic");
		for(Resource resource : resources) {
			dictionary.addDictionary(new InputStreamReader(resource.getInputStream()));
		}
	}
	
	public boolean addToDictionary(String word) {
		return spellChecker.addToDictionary(word);
	}
	
	@SuppressWarnings("unchecked")
	public String correct(String word) {
		if(StringUtils.hasText(word) && !hasDigits(word) && !spellChecker.isCorrect(word)) {
			final List<Word> suggestions = spellChecker.getSuggestions(word, 1);
			return suggestions.isEmpty() ? word : suggestions.get(0).getWord(); 
		}
		return word;
	}
	
	private boolean hasDigits(String text) {
		if (null != text) {
			for (int index = 0; index < text.length(); index++) {
				if (Character.isDigit(text.charAt(index))) {
					return true;
				}
			}
		}
		return false;
	}
	
}
