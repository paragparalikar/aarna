package com.aarna.chat.nlp;

import java.io.IOException;
import java.io.ObjectInputStream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;

import edu.stanford.nlp.classify.ColumnDataClassifier;
import edu.stanford.nlp.ling.Datum;
import edu.stanford.nlp.stats.Counter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class IntentRecognitionService {

	@Autowired private AmazonS3 amazonS3Client;
	
	@Value("${aarna.nlp.intent.classifier-bucket:aarna-nlp}")
	private String classifierBucket;
	
	@Value("${aarna.nlp.intent.classifier-object-name:intent.classifier.ser}")
	private String classifierObjectName;
	
	@Value("${aarna.nlp.intent.trainer-config-path:classpath:nlp/intent.trainer.properties}")
	private Resource configPath;
	
	private volatile ColumnDataClassifier columnDataClassifier;
	
	@PostConstruct
	public void init() throws IOException, ClassNotFoundException {
		final S3Object s3Object = amazonS3Client.getObject(classifierBucket, classifierObjectName);
		try(final S3ObjectInputStream s3InputStream = s3Object.getObjectContent()){
			final ObjectInputStream objectInputStream = new ObjectInputStream(s3InputStream);
			columnDataClassifier = ColumnDataClassifier.getClassifier(objectInputStream);
			log.debug("Loaded classifier for intent recognition");
		}
	}
	
	public String recognize(String text) throws IOException {
		final String[] data = new String[] {"unknown", text.toLowerCase()};
		final Datum<String, String> datum = columnDataClassifier.makeDatumFromStrings(data);
		final String intent = columnDataClassifier.classOf(datum);
		final Counter<String> counter = columnDataClassifier.scoresOf(datum);
		final double score = counter.getCount(intent);
		log.info("Recognized intent {} with score {} for text {}", intent, score, text);
		return intent;
	}
	
}
