package com.aarna.chat.nlp.similarity;

import org.apache.commons.text.similarity.JaroWinklerDistance;
import org.apache.commons.text.similarity.SimilarityScore;
import org.springframework.stereotype.Component;

import lombok.experimental.Delegate;

@Component
public class EditDistanceSimilarity implements SimilarityScore<Double> {
	
	@Delegate
	private final JaroWinklerDistance delegate = new JaroWinklerDistance();

}
