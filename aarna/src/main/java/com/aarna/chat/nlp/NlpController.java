package com.aarna.chat.nlp;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/nlp")
public class NlpController {

	@Autowired private IntentRecognitionService intentRecognitionService;
	
	@GetMapping("/intents")
	public String findIntent(@RequestParam("q") final String query) throws IOException {
		return intentRecognitionService.recognize(query);
	}
	
	@PostMapping("/intents")
	public void trainIntentRecognition() throws IOException, ClassNotFoundException {
		intentRecognitionService.init();
	}
}
