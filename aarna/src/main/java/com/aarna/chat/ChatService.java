package com.aarna.chat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aarna.chat.nlp.IntentRecognitionService;
import com.aarna.chat.nlp.TextNormalizer;
import com.aarna.chat.responder.ChatResponder;
import com.aarna.chat.responder.ChatResponderFactory;

@Service
public class ChatService {

	@Autowired private TextNormalizer textNormalizer;
	@Autowired private ChatResponderFactory chatResponderFactory;
	@Autowired private IntentRecognitionService intentRecognitionService;
	
	public void service(final ChatRequest request, final ChatResponse response) throws Exception {
		request.setText(textNormalizer.apply(request.getMessage()));
		request.setIntent(intentRecognitionService.recognize(request.getMessage()));
		final ChatResponder responder = chatResponderFactory.getResponder(request.getIntent());
		responder.respond(request, response);
	}
	
}
