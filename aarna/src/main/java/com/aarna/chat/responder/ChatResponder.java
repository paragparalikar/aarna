package com.aarna.chat.responder;

import com.aarna.chat.ChatRequest;
import com.aarna.chat.ChatResponse;

public interface ChatResponder {

	public void respond(final ChatRequest request, final ChatResponse response) throws Exception;
	
}
