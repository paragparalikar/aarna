package com.aarna.chat.responder;

import org.springframework.stereotype.Component;

import com.aarna.chat.ChatRequest;
import com.aarna.chat.ChatResponse;

@Component
public class CostQueryResponder implements ChatResponder {

	@Override
	public void respond(ChatRequest request, ChatResponse response) throws Exception {
		response.setMessage("It's free !!!");
	}

}
