package com.aarna.chat.responder;

import static com.aarna.AarnaConstants.INTENT_CHAT_START;

import org.springframework.stereotype.Component;

import com.aarna.chat.ChatRequest;
import com.aarna.chat.ChatResponse;
import com.aarna.chat.ChatSession;

@Component
public class ChatStartResponder implements ChatResponder {

	@Override
	public void respond(ChatRequest request, ChatResponse response) throws Exception {
		final ChatSession chatSession = request.getChatSession();
		response.setMessage(chatSession.hasIntent(INTENT_CHAT_START) ? "Hi" : 
			"Hello, what topic shall we learn today ?");
	}

}
