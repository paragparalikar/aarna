package com.aarna.chat.responder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.aarna.AarnaConstants;

@Component
public class ChatResponderFactory {

	@Autowired private ChatStartResponder startResponder;
	@Autowired private ChatStopResponder stopResponder;
	@Autowired private ContentQueryResponder contentQueryResponder;
	@Autowired private CostQueryResponder costQueryResponder;
	
	public ChatResponder getResponder(String intent) {
		switch(intent) {
		case AarnaConstants.INTENT_CHAT_START: return startResponder;
		case AarnaConstants.INTENT_CHAT_STOP: return stopResponder;
		case AarnaConstants.INTENT_QUERY_CONTENT: return contentQueryResponder;
		case AarnaConstants.INTENT_QUERY_COST: return costQueryResponder;
		case AarnaConstants.INTENT_CHAT_AFFERMATION:
		case AarnaConstants.INTENT_CHAT_CONTINUE:
		case AarnaConstants.INTENT_CHAT_PAUSE:
		case AarnaConstants.INTENT_CHAT_REJECTION:
		case AarnaConstants.INTENT_CHAT_RESUME:return NoopResponder.INSTANCE;
		default: return contentQueryResponder;
		}
	}
	
}
