package com.aarna.chat.responder;

import java.util.LinkedList;
import java.util.List;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "urls")
public class RecommendationItem {
	private final String topic;
	private Double score = Double.MIN_VALUE;
	private final List<String> urls = new LinkedList<>();
	
	public String toDisplayString() {
		final StringBuilder builder = new StringBuilder("\n");
		builder.append(topic).append("\n");
		urls.stream()
			.distinct()
			.sorted()
			.forEach(url -> builder.append(url).append("\n"));
		return builder.toString();
	}
}