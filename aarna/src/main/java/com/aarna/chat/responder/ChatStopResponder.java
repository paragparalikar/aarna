package com.aarna.chat.responder;

import org.springframework.stereotype.Component;

import com.aarna.AarnaConstants;
import com.aarna.chat.ChatRequest;
import com.aarna.chat.ChatResponse;
import com.aarna.chat.ChatSession;

@Component
public class ChatStopResponder implements ChatResponder {

	@Override
	public void respond(ChatRequest request, ChatResponse response) throws Exception {
		final ChatSession chatSession = request.getChatSession();
		if(!chatSession.hasIntent(AarnaConstants.INTENT_CHAT_STOP)) {
			response.setMessage("It was nice chatting with you, bye !");
		}
	}

}
