package com.aarna.chat.responder;

import com.aarna.chat.ChatRequest;
import com.aarna.chat.ChatResponse;


public final class NoopResponder implements ChatResponder {

	public static final NoopResponder INSTANCE = new NoopResponder();
	
	private NoopResponder() {}

	@Override
	public void respond(ChatRequest request, ChatResponse response) throws Exception {
		
	}

}
