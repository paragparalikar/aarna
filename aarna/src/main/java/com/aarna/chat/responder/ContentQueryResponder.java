package com.aarna.chat.responder;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.aarna.chat.ChatRequest;
import com.aarna.chat.ChatResponse;
import com.aarna.content.Content;
import com.aarna.recommendation.Recommendation;
import com.aarna.recommendation.RecommendationRequest;
import com.aarna.recommendation.RecommendationResponse;
import com.aarna.recommendation.RecommendationService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ContentQueryResponder implements ChatResponder {
	
	@Autowired private RecommendationService recommendationService;
	
	@Value("${aarna.recommendation.max-count:3}")
	private Integer maxRecommendations;

	@Override
	public void respond(ChatRequest request, ChatResponse response) throws Exception {
		final RecommendationRequest recommendationRequest = RecommendationRequest.builder()
				.normalizedText(request.getText())
				.profile(request.getChatSession().getProfile())
				.limit(maxRecommendations)
				.build();
		
		final RecommendationResponse recommendationResponse = recommendationService
				.recommend(recommendationRequest);
		
		final List<Recommendation> recommendations = recommendationResponse.getRecommendations();
		if(recommendations.isEmpty()) {
			log.error("No recommendations found for input {}", request.getMessage());
			response.setMessage("I could not find anything for that. Can you please rephrase your question?");
		} else {
			final Map<String, RecommendationItem> items = new HashMap<>();
			for(Recommendation recommendation : recommendations) {
				final Content content = recommendation.getContent();
				final RecommendationItem item = items.computeIfAbsent(content.toDisplayString(), RecommendationItem::new);
				item.getUrls().add(content.getUrl());
				item.setScore(Math.max(item.getScore(), recommendation.getScore()));
			}
			response.setMessage(String.format("I found %d matching items", items.size()));
			items.values().stream()
				.sorted(Comparator.comparing(RecommendationItem::getScore).reversed())
				.forEach(response.getRecommendations()::add);
		}
	}
}
