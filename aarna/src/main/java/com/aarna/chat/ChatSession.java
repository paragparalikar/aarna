package com.aarna.chat;

import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.function.Predicate;

import com.aarna.profile.model.Profile;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class ChatSession {
	
	private final String id;
	private final transient Profile profile;
	private final LinkedHashMap<String, ChatRequest> requests = new LinkedHashMap<>();
	private final LinkedHashMap<String, ChatResponse> responses = new LinkedHashMap<>();
	
	public ChatSession(Profile profile) {
		this(UUID.randomUUID().toString(), profile);
	}
	
	public boolean hasIntent(String intent) {
		return requests.values().stream()
				.map(ChatRequest::getIntent)
				.anyMatch(Predicate.isEqual(intent));
	}
	
}
