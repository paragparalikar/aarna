package com.aarna.profile.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTyped;

import lombok.Data;

@Data
@DynamoDBDocument
public class Parent {

	private String name;
	private String email;
	private String phone;

	@DynamoDBTyped(DynamoDBAttributeType.M)
	private Address address;
	private boolean addressSameAsStudent;
	
}
