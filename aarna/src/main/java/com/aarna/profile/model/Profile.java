package com.aarna.profile.model;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperFieldModel.DynamoDBAttributeType;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTyped;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@DynamoDBTable(tableName = "profile")
public class Profile {
	
	private String name;
	private String board;
	private String email;
	private String phone;
	private Integer standard;
	private String photo;
	private String identityCard;
	private String identityCardType;
	private String about;
	private String awards;
	private String competition;
	
	@DynamoDBHashKey private String username;
	@DynamoDBTyped(DynamoDBAttributeType.M) private Parent parent;
	@DynamoDBTyped(DynamoDBAttributeType.M) private Address address;
	@DynamoDBTyped(DynamoDBAttributeType.S) private ProfileType type;
	@DynamoDBTyped(DynamoDBAttributeType.S) private Date dateOfBirth;
	@DynamoDBTyped(DynamoDBAttributeType.M) private Map<String, Integer> expertises;
	@DynamoDBTyped(DynamoDBAttributeType.SS) private Set<@NotBlank @Size(min = 1, max = 256) String> hobbies;
	@DynamoDBTyped(DynamoDBAttributeType.SS) private Set<@NotBlank @Size(min = 1, max = 256) String> fovouriteSubjects;
	
}
