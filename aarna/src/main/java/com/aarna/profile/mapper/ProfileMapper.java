package com.aarna.profile.mapper;

import java.io.IOException;
import java.net.URL;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.MultipartFile;

import com.aarna.common.amazon.AmazonS3Template;
import com.aarna.profile.dto.ProfileDto;
import com.aarna.profile.dto.ProfileRequestDto;
import com.aarna.profile.dto.ProfileResponseDto;
import com.aarna.profile.model.Profile;

@Mapper(componentModel = "spring", uses = {AddressMapper.class, ParentMapper.class})
public abstract class ProfileMapper {
	
	@Autowired private AmazonS3Template s3Template;
	@Value("${aarna.profile.bucket.photoes:aarna-photoes}") private String photoesBucketName;
	@Value("${aarna.profile.bucket.identity-cards:aarna-identity-cards}") private String identityCardsBucketName;
	
	@Mapping(target = "photo", ignore = true)
	@Mapping(target = "identityCard", ignore = true)
	public abstract ProfileRequestDto toRequestDto(ProfileDto dto);
	
	@Mapping(target = "photo", source = "photo", qualifiedByName = "mapPhotoToKey")
	@Mapping(target = "identityCard", source = "identityCard", qualifiedByName = "mapIdentityCardToKey")
	public abstract Profile fromDto(ProfileRequestDto dto);

	@Mapping(target = "photo", source = "photo", qualifiedByName = "mapPhotoToPresignedUrl")
	@Mapping(target = "identityCard", source = "identityCard", qualifiedByName = "mapIdentityCardToPresignedUrl")
	public abstract ProfileResponseDto toDto(Profile profile);
	
	@Named("mapPhotoToKey")
	public String mapPhotoToKey(MultipartFile file) throws IOException {
		return s3Template.upload(photoesBucketName, file);
	}
	
	@Named("mapPhotoToPresignedUrl")
	public URL mapPhoto(String photoKey) {
		return s3Template.presignedUrl(photoesBucketName, photoKey, 1);
	}
	
	@Named("mapIdentityCardToKey")
	public String mapIdentityCardToKey(MultipartFile file) throws IOException {
		return s3Template.upload(identityCardsBucketName, file);
	}
	
	@Named("mapIdentityCardToPresignedUrl")
	public URL mapIdentityCard(String identityCardKey) {
		return s3Template.presignedUrl(identityCardsBucketName, identityCardKey, 1);
	}
}
