package com.aarna.profile.mapper;

import org.mapstruct.Mapper;

import com.aarna.profile.dto.AddressDto;
import com.aarna.profile.model.Address;

@Mapper(componentModel = "spring")
public interface AddressMapper {

	Address fromDto(AddressDto dto);
	
}
