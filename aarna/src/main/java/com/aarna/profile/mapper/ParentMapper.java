package com.aarna.profile.mapper;

import org.mapstruct.Mapper;

import com.aarna.profile.dto.ParentDto;
import com.aarna.profile.model.Parent;

@Mapper(componentModel = "spring", uses = AddressMapper.class)
public interface ParentMapper {

	Parent fromDto(ParentDto dto);
	
}
