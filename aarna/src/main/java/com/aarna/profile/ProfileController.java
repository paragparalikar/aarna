package com.aarna.profile;

import static org.springframework.http.HttpStatus.UNAUTHORIZED;
import static org.springframework.http.MediaType.MULTIPART_FORM_DATA_VALUE;

import java.io.IOException;
import java.util.Objects;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.AuthenticatedPrincipal;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import com.aarna.profile.dto.ProfileDto;
import com.aarna.profile.dto.ProfileRequestDto;
import com.aarna.profile.dto.ProfileResponseDto;
import com.aarna.profile.mapper.ProfileMapper;
import com.aarna.profile.model.Profile;

@RestController
@RequestMapping("/api/v1/profiles")
public class ProfileController {

	@Autowired private ProfileMapper profileMapper;
	@Autowired private ProfileService profileService;
	
	@GetMapping("/self")
	public ProfileResponseDto self(@AuthenticationPrincipal final AuthenticatedPrincipal principal) {
		final Profile profile = profileService.findByUsername(principal.getName());
		return profileMapper.toDto(profile);
	}
	
	@PutMapping(path = "/self", consumes = MULTIPART_FORM_DATA_VALUE)
	public ProfileResponseDto update(
			@RequestPart("profile") @Valid final ProfileDto dto,
			@RequestPart(name = "photo", required = false) final MultipartFile photo,
			@RequestPart(name = "identityCard", required = false) final MultipartFile identityCard,
			@AuthenticationPrincipal final AuthenticatedPrincipal principal) throws IOException {
		if(!Objects.equals(principal.getName(), dto.getUsername())) {
			final String message = String.format("You (%s) are not authorized to edit user profile of %s", principal.getName(), dto.getUsername());
			throw new ResponseStatusException(UNAUTHORIZED, message);
		}
		final ProfileRequestDto requestDto = profileMapper.toRequestDto(dto);
		requestDto.setPhoto(photo);
		requestDto.setIdentityCard(identityCard);
		final Profile profile = profileMapper.fromDto(requestDto);
		final Profile result = profileService.save(profile);
		return profileMapper.toDto(result);
	}

}
