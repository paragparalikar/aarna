package com.aarna.profile.dto;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.aarna.profile.model.Address;

import lombok.Data;

@Data
public class ParentDto {

	@NotBlank
	@Size(max = 256)	
	private String name;
	
	@Email
	@NotBlank
	@Size(max = 256)	
	private String email;
	
	@Size(max = 256)	
	private String phone;
	
	@Valid
	private Address address;
	
	private boolean addressSameAsStudent;
}
