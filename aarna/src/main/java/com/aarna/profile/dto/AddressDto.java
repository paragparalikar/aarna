package com.aarna.profile.dto;

import javax.validation.constraints.Size;

import lombok.Data;

@Data
public class AddressDto {

	@Size(max = 256) private String addressLine1;
	@Size(max = 256) private String addressLine2;
	@Size(max = 256) private String city;
	@Size(max = 256) private String state;
	@Size(max = 256) private String country;
	@Size(max = 256) private String pin;
	
}
