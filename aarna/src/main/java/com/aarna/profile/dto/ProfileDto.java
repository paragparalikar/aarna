package com.aarna.profile.dto;

import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import com.aarna.profile.model.Address;
import com.aarna.profile.model.Parent;
import com.aarna.profile.model.ProfileType;

import lombok.Data;

@Data
public class ProfileDto {

	@Size(min = 1, max = 256) @NotBlank private String name;
	@Size(min = 1, max = 128) @NotBlank private String username;
	@Size(max = 256) private String board;
	@Min(6) @Max(12) private Integer standard;
	@Past private Date dateOfBirth;
	@Size(max = 256) @NotBlank @Email private String email;
	@Size(max = 32)	private String phone;
	@NotNull private ProfileType type;
	@Size(max = 256) private String identityCardType;
	@Size(max = 256) private String about;
	@Size(max = 256) private String awards;
	@Size(max = 256) private String competition;
	@Valid private Address address;
	@Valid private Parent parent;
	@Size(max = 2) private Map<String, Integer> expertises;
	@Size(max = 2) private Set<@NotBlank @Size(min = 1, max = 256) String> hobbies;
	@Size(max = 2) private Set<@NotBlank @Size(min = 1, max = 256) String> fovouriteSubjects;
	
}
