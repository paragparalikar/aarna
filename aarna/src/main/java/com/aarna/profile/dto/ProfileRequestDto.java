package com.aarna.profile.dto;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProfileRequestDto extends ProfileDto {

	private MultipartFile photo;
	private MultipartFile identityCard;
	
	
}
