package com.aarna.profile.dto;

import java.net.URL;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ProfileResponseDto extends ProfileDto {

	private URL photo;
	private URL identityCard;
	
}
