package com.aarna.profile.event;

import com.aarna.profile.model.Profile;

import lombok.Value;

@Value
public class ProfileUpdatedEvent {

	private final Profile oldValue;
	private final Profile newValue;
	
}
