package com.aarna.profile.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.aarna.common.amazon.AmazonS3Template;
import com.aarna.profile.model.Profile;

@Component
public class ProfileEventHandler {
	
	@Autowired private AmazonS3Template s3Template;
	@Value("${aarna.profile.bucket.photoes:aarna-photoes}") private String photoesBucketName;
	@Value("${aarna.profile.bucket.identity-cards:aarna-identity-cards}") private String identityCardsBucketName;
	
	@EventListener
	public void onProfileDeleted(ProfileDeletedEvent event) {
		deleteObjects(event.getProfile());
	}

	@EventListener
	public void onProfileUpdated(ProfileUpdatedEvent event) {
		deleteObjects(event.getOldValue());
	}
	
	private void deleteObjects(Profile profile) {
		s3Template.delete(photoesBucketName, profile.getPhoto());
		s3Template.delete(identityCardsBucketName, profile.getIdentityCard());
	}
	
}
