package com.aarna.profile.event;

import com.aarna.profile.model.Profile;

import lombok.Value;

@Value
public class ProfileDeletedEvent {

	private final Profile profile;
	
}
