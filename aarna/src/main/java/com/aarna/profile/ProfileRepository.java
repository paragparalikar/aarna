package com.aarna.profile;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.repository.PagingAndSortingRepository;

import com.aarna.profile.model.Profile;

@EnableScan
@EnableScanCount
public interface ProfileRepository extends PagingAndSortingRepository<Profile, String> {

}
