package com.aarna.profile;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.aarna.common.web.JsonHttpMessageConverter;
import com.aarna.profile.dto.ProfileDto;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class ProfileWebConfigurer implements WebMvcConfigurer {
	
	@Autowired private ObjectMapper objectMapper;

	@Override
	public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
		converters.add(new JsonHttpMessageConverter<>(ProfileDto.class, objectMapper));
	}
	
}
