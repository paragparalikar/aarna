package com.aarna.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.aarna.profile.event.ProfileDeletedEvent;
import com.aarna.profile.event.ProfileUpdatedEvent;
import com.aarna.profile.model.Profile;

@Service
public class ProfileService {

	@Autowired private ProfileRepository profileRepository;
	@Autowired private ApplicationEventPublisher eventPublisher;
	
	public Page<Profile> findAll(Pageable pageable){
		return profileRepository.findAll(pageable);
	}
	
	public boolean existsByUsername(String username) {
		return profileRepository.existsById(username);
	}
	
	public Profile findByUsername(String username) {
		return profileRepository.findById(username).orElse(null);
	}
	
	public Profile save(Profile profile) {
		final Profile oldValue = findByUsername(profile.getUsername());
		profile.setEmail(oldValue.getEmail());
		profile.setPhone(oldValue.getPhone());
		final Profile newValue = profileRepository.save(profile);
		eventPublisher.publishEvent(new ProfileUpdatedEvent(oldValue, newValue));
		return newValue;
	}
	
	public void deleteByUsername(String username) {
		profileRepository.findById(username).ifPresent(profile -> {
			profileRepository.delete(profile);
			eventPublisher.publishEvent(new ProfileDeletedEvent(profile));
		});
	}
}
