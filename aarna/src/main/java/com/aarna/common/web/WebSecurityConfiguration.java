package com.aarna.common.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired private ProfileAwareAuthenticationSuccessHandler successHandler;
	
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.csrf().disable()
        	.cors().disable()
        	.authorizeRequests()
        		.antMatchers("/").permitAll()
        		.anyRequest().authenticated().and()
        	.oauth2Login()
        		.loginPage("/")
        		.successHandler(successHandler).and()
        	.oauth2ResourceServer()
        		.jwt().and().and()
        	.logout()
        		.deleteCookies("JSESSIONID")
        		.invalidateHttpSession(true);
    }

}
