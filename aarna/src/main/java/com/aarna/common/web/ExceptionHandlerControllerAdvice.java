package com.aarna.common.web;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.core.NestedExceptionUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlerControllerAdvice extends ResponseEntityExceptionHandler {

	@ExceptionHandler
	public ResponseEntity<Object> handle(Exception e) {
		return handle(e, HttpStatus.INTERNAL_SERVER_ERROR, null);
	}
	
	@Override
	public ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		log.error("", ex);
		final List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
	    final Map <String, Set<String>> errorsMap =  fieldErrors.stream().collect(Collectors.groupingBy(FieldError::getField, 
	    		Collectors.mapping(FieldError::getDefaultMessage, Collectors.toSet())));
	    return new ResponseEntity<>(errorsMap.isEmpty() ? ex : errorsMap, headers, HttpStatus.BAD_REQUEST);
	}
	
	private ResponseEntity<Object> handle(Exception e, HttpStatus httpStatus, String message){
		log.error("", e);
		if(!StringUtils.hasText(message)) {
			final Throwable root = NestedExceptionUtils.getMostSpecificCause(e);
			message = String.join(" - ", root.getClass().getSimpleName(), root.getMessage()); 
		}
		return ResponseEntity
				.status(httpStatus)
				.body(message);
	}
}
