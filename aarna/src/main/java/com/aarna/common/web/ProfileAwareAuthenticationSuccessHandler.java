package com.aarna.common.web;

import static com.aarna.AarnaConstants.CHAT;
import static com.aarna.AarnaConstants.PROFILE;

import java.io.IOException;
import java.util.Optional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.aarna.chat.ChatSession;
import com.aarna.profile.ProfileService;
import com.aarna.profile.model.Profile;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class ProfileAwareAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	@Autowired private ProfileService profileService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
		final OAuth2AuthenticationToken token = (OAuth2AuthenticationToken) authentication;
		log.info("User {} has been successfully authenticated", token.getName());
		final Profile profile = createIfAbsent(profileService.findByUsername(token.getName()), token);
		request.getSession().setAttribute(PROFILE, profile);
		request.getSession().setAttribute(CHAT, new ChatSession(profile));
		super.onAuthenticationSuccess(request, response, authentication);
	}
	
	private Profile createIfAbsent(Profile profile, OAuth2AuthenticationToken token) {
		if(null == profile) {
			log.info("User {} does not have a profile, one will be created", token.getName());
			final OidcUser user = (OidcUser) token.getPrincipal();
			profile = Profile.builder()
					.username(token.getName())
					.name(Optional.ofNullable(user.getFullName()).orElse(user.getName()))
					.email(user.getEmail())
					.build();
			log.info("Attempting to persist {}", profile);
			profile = profileService.save(profile);
			log.info("Persisted {}", profile);
		}
		return profile;
	}
	
}
