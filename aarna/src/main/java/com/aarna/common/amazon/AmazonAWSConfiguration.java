package com.aarna.common.amazon;

import org.socialsignin.spring.data.dynamodb.config.EnableDynamoDBAuditing;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.aarna.AarnaApplication;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;

@Configuration
@EnableDynamoDBAuditing
@EnableDynamoDBRepositories(basePackageClasses = AarnaApplication.class)
public class AmazonAWSConfiguration {
	
	@Bean
	public AmazonS3 amazonS3(
			final AWSCredentialsProvider credentialsProvider,
			@Value("${amazon.s3.endpoint:s3.ap-south-1.amazonaws.com}") final String endpoint,
    		@Value("${amazon.s3.signing-region:ap-south-1}") final String signingRegion) {
    	final EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, signingRegion);
		return AmazonS3ClientBuilder.standard()
				.withCredentials(credentialsProvider)
				.withEndpointConfiguration(endpointConfiguration)
				.build();
	}
	
    @Bean
    public AmazonDynamoDB amazonDynamoDB(
    		final AWSCredentialsProvider credentialsProvider,
    		@Value("${amazon.dynamodb.endpoint:dynamodb.ap-south-1.amazonaws.com}") final String endpoint,
    		@Value("${amazon.dynamodb.signing-region:ap-south-1}") final String signingRegion) {
    	final EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, signingRegion);
    	return AmazonDynamoDBClientBuilder
    			.standard()
    			.withCredentials(credentialsProvider)
    			.withEndpointConfiguration(endpointConfiguration)
    			.build();
    }
    
    @Bean
    public AWSCredentialsProvider awsCredentialsProvider(
    		@Value("${amazon.aws.accesskey}") final String amazonAWSAccessKey,
    		@Value("${amazon.aws.secretkey}") final String amazonAWSSecretKey) {
    	final BasicAWSCredentials credentials = new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
    	final AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
    	return credentialsProvider;
    }

}


