package com.aarna.common.amazon;

import java.io.IOException;
import java.net.URL;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;

import lombok.NonNull;

@Component
public class AmazonS3Template {

	@Autowired private AmazonS3 s3Client;
	
	public URL presignedUrl(@NonNull String bucketName, String key, int validityInMinutes) {
		if(!StringUtils.hasText(key)) return null;
		final Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MINUTE, validityInMinutes);
		final Date expiry = calendar.getTime();
		return s3Client.generatePresignedUrl(bucketName, key, expiry, HttpMethod.GET);
	}
	
	public String upload(@NonNull String bucketName, MultipartFile file) throws IOException {
		if(null == file) return null;
		final String key = UUID.randomUUID().toString();
		final ObjectMetadata objectMetadata = new ObjectMetadata();
		objectMetadata.setContentLength(file.getSize());
		objectMetadata.setContentType(file.getContentType());
		final PutObjectRequest request = new PutObjectRequest(bucketName, key, 
				file.getInputStream(), objectMetadata);
		s3Client.putObject(request);
		return key;
	}
	
	public void delete(@NonNull String bucketName, String key) {
		if(null == key) return;
		s3Client.deleteObject(bucketName, key);
	}
	
}
