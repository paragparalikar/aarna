package com.aarna.recommendation.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.aarna.content.Content;
import com.aarna.recommendation.RecommendationRequest;

@Primary
@Component
public class CompositeRecommendationContentFilter implements RecommendationContentFilter {

	@Autowired private BoardEqualityRecommendationContentFilter boardFilter;
	@Autowired private StandardEqualityRecommendationContentFilter standardFilter;
	
	@Override
	public boolean test(RecommendationRequest request, Content content) {
		return  boardFilter.test(request, content) && 
				standardFilter.test(request, content);
	}

}
