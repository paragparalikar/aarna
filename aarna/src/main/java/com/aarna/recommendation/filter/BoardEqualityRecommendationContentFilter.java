package com.aarna.recommendation.filter;

import java.util.Objects;

import org.springframework.stereotype.Component;

import com.aarna.content.Content;
import com.aarna.recommendation.RecommendationRequest;

@Component
public class BoardEqualityRecommendationContentFilter implements RecommendationContentFilter {

	@Override
	public boolean test(RecommendationRequest request, Content content) {
		return  null != request && 
				null != request.getProfile() &&
				null != content &&
				Objects.equals(request.getProfile().getBoard(), content.getBoard());
	}

}
