package com.aarna.recommendation.filter;

import java.util.function.BiPredicate;

import com.aarna.content.Content;
import com.aarna.recommendation.RecommendationRequest;

public interface RecommendationContentFilter extends BiPredicate<RecommendationRequest, Content> {

}
