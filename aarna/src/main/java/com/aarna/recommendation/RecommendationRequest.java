package com.aarna.recommendation;

import com.aarna.chat.nlp.NormalizedText;
import com.aarna.profile.model.Profile;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RecommendationRequest {

	private final int limit;
	private final Profile profile;
	private final NormalizedText normalizedText;
	
}
