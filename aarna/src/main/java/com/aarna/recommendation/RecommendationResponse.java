package com.aarna.recommendation;

import java.util.List;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RecommendationResponse {

	private final List<Recommendation> recommendations;
	
}
