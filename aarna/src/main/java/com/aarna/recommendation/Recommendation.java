package com.aarna.recommendation;

import com.aarna.content.Content;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Recommendation {

	private String id;
	private Double score;
	private Content content;
	
}
