package com.aarna.recommendation;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aarna.chat.nlp.NormalizedText;
import com.aarna.chat.nlp.similarity.FuzzyMatchCountSimilarity;
import com.aarna.recommendation.ContentTagsIndex.IndexKey;
import com.aarna.recommendation.filter.RecommendationContentFilter;

@Service
public class RecommendationService {

	@Autowired private ContentTagsIndex contentTagIndex;
	@Autowired private RecommendationContentFilter contentFilter;
	@Autowired private FuzzyMatchCountSimilarity matchCountSimilarity;
	
	public RecommendationResponse recommend(RecommendationRequest request){
		final List<Recommendation> primaryRecommendations = recommend(IndexKey::getPrimaryText, request);
		if(request.getLimit() > primaryRecommendations.size()) {
			final List<Recommendation> secondaryRecommendations = recommend(IndexKey::getSecondaryText, request);
			final List<Recommendation> mergedRecommendations = Stream.concat(primaryRecommendations.stream(), secondaryRecommendations.stream())
					.sorted(Comparator.comparing(Recommendation::getScore).reversed())
					.limit(request.getLimit())
					.collect(Collectors.toList());
			return RecommendationResponse.builder()
					.recommendations(mergedRecommendations)
					.build();
		}
		return RecommendationResponse.builder()
				.recommendations(primaryRecommendations)
				.build();
	}
	
	private List<Recommendation> recommend(Function<IndexKey, NormalizedText> inputExtractor, RecommendationRequest request){
		return score(inputExtractor, request).entrySet().stream()
			.sorted((e1,e2) -> Comparator.<Double>reverseOrder().compare(e1.getValue(), e2.getValue()))
			.limit(request.getLimit())
			.flatMap(entry -> transform(entry, request))
			.collect(Collectors.toList());
	}
	
	private Map<IndexKey, Double> score(Function<IndexKey, NormalizedText> inputExtractor, RecommendationRequest request){
		final NormalizedText text = request.getNormalizedText();
		return contentTagIndex.keySet().stream()
				.collect(Collectors.toMap(Function.identity(), 
					key -> score(inputExtractor.apply(key), text)));
	}
	
	private Double score(final NormalizedText input, final NormalizedText text) {
		Double score = matchCountSimilarity.apply(text.getTokens(), input.getTokens());
		if(0 < score) {
			score += matchCountSimilarity.apply(text.getOriginalText(), input.getOriginalText());
			score += matchCountSimilarity.apply(text.getNormalizedText(), input.getNormalizedText());
		}
		return score;
	}
	
	private Stream<Recommendation> transform(Entry<IndexKey, Double> entry, RecommendationRequest request){
		return contentTagIndex.get(entry.getKey()).stream()
				.filter(content -> contentFilter.test(request, content))
				.map(content -> Recommendation.builder()
						.id(UUID.randomUUID().toString())
						.score(entry.getValue())
						.content(content)
						.build());
	}
	
	
	
}
