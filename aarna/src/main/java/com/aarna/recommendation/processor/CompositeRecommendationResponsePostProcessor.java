package com.aarna.recommendation.processor;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.aarna.recommendation.RecommendationRequest;
import com.aarna.recommendation.RecommendationResponse;

@Primary 
@Component
public class CompositeRecommendationResponsePostProcessor implements RecommendationResponsePostProcessor {

	@Override
	public RecommendationResponse apply(RecommendationRequest request, RecommendationResponse response) {
		return response;
	}

}
