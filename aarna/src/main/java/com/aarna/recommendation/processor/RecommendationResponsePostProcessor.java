package com.aarna.recommendation.processor;

import java.util.function.BiFunction;

import com.aarna.recommendation.RecommendationRequest;
import com.aarna.recommendation.RecommendationResponse;

public interface RecommendationResponsePostProcessor extends BiFunction<RecommendationRequest, 
	RecommendationResponse, RecommendationResponse> {

}
