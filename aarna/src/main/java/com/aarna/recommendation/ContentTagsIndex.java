package com.aarna.recommendation;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import com.aarna.chat.nlp.NormalizedText;
import com.aarna.chat.nlp.TextNormalizer;
import com.aarna.content.Content;
import com.aarna.content.ContentService;
import com.aarna.content.event.ContentOffboardedEvent;
import com.aarna.content.event.ContentOnboardedEvent;
import com.aarna.recommendation.ContentTagsIndex.IndexKey;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

@Component
public class ContentTagsIndex extends HashMap<IndexKey, Set<Content>> {
	private static final long serialVersionUID = -2467941859869502886L;

	@Value
	@Builder
	@EqualsAndHashCode(of = "primaryText")
	public static final class IndexKey {
		@NonNull private final NormalizedText primaryText;
		@NonNull private final NormalizedText secondaryText;
	}
	
	@Autowired private ContentService contentService;
	@Autowired private TextNormalizer textNormalizer;
	
	@PostConstruct
	public void init() throws IOException {
		for(Content content : contentService.findAll()) index(content);
	}
	
	@EventListener
	public void onContentOnboarded(ContentOnboardedEvent event) {
		index(event.getContent());
	}
	
	@EventListener
	public void onContentOffboarded(ContentOffboardedEvent event) {
		unindex(event.getContent());
	}
	
	private void index(Content content) {
		final NormalizedText primaryText = textNormalizer.apply(String.join(" ", 
				content.getSubject(), content.getTopic(), content.getSubtopic()));
		final NormalizedText secondaryText = textNormalizer.apply(String.join(" ", content.getTags()));
		final IndexKey key = IndexKey.builder().primaryText(primaryText).secondaryText(secondaryText).build(); 
		computeIfAbsent(key, k -> new HashSet<>()).add(content);
	}
	
	private void unindex(Content content) {
		final Iterator<Entry<IndexKey, Set<Content>>> iterator = entrySet().iterator();
		while(iterator.hasNext()) {
			final Entry<IndexKey, Set<Content>> entry = iterator.next();
			entry.getValue().remove(content);
			if(entry.getValue().isEmpty()) {
				iterator.remove();
			}
		}
	}
	
}
