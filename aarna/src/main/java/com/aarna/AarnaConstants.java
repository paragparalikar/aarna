package com.aarna;

import lombok.experimental.UtilityClass;

@UtilityClass
public class AarnaConstants {

	public static final String INTENT_CHAT_START = "chat_start";
	public static final String INTENT_CHAT_STOP = "chat_stop";
	public static final String INTENT_CHAT_PAUSE = "chat_pause";
	public static final String INTENT_CHAT_RESUME = "chat_resume";
	public static final String INTENT_CHAT_CONTINUE = "chat_continue";
	public static final String INTENT_CHAT_AFFERMATION = "chat_affermation";
	public static final String INTENT_CHAT_REJECTION = "chat_rejection";
	public static final String INTENT_QUERY_CONTENT = "query_content";
	public static final String INTENT_QUERY_COST = "query_cost";
	public static final String INTENT_UNKNOWN = "unknown";
	
	public static final String CHAT = "aarna.key.chat";
	public static final String PROFILE = "aarna.key.profile";
	
	
}
