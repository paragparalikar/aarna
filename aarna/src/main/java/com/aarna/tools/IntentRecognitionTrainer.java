package com.aarna.tools;

import java.io.FileInputStream;
import java.util.Properties;

import edu.stanford.nlp.classify.ColumnDataClassifier;

public class IntentRecognitionTrainer {

	public static void main(String[] args) throws Exception {
		final String configPath = "C:\\git\\aarna\\aarna\\src\\main\\resources\\nlp\\intent.trainer.properties";
		final String trainingDataPath = "C:\\applications\\aarna\\intent.train.tsv";
		final String classifierPath = "C:\\applications\\aarna\\intent.classifier.ser";
		final Properties properties = new Properties();
		properties.load(new FileInputStream(configPath));
		final ColumnDataClassifier columnDataClassifier = new ColumnDataClassifier(properties);
		columnDataClassifier.trainClassifier(trainingDataPath);
		columnDataClassifier.serializeClassifier(classifierPath.toString());
	}
	
}
