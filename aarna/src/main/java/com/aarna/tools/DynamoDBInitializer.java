package com.aarna.tools;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.util.StringUtils;

import com.aarna.content.Content;
import com.aarna.content.ContentType;
import com.aarna.profile.model.Profile;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.CreateTableResult;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.Select;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DynamoDBInitializer {
	
	private final String endpoint = "dynamodb.ap-south-1.amazonaws.com";
	private final String signingRegion = "ap-south-1";
	private final String amazonAWSAccessKey = "AKIAZWDJQWVDMRAVBCFS";
	private final String amazonAWSSecretKey = "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";
	private final Path path = Paths.get("C:\\Users\\parag\\Downloads\\aarna\\aarna.json");
	private final BasicAWSCredentials credentials = new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
	private final AWSStaticCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(credentials);
	private final EndpointConfiguration endpointConfiguration = new EndpointConfiguration(endpoint, signingRegion);
	private final AmazonDynamoDB database = AmazonDynamoDBClientBuilder.standard().withCredentials(credentialsProvider)
			.withEndpointConfiguration(endpointConfiguration).build();
	private final DynamoDBMapper mapper = new DynamoDBMapper(database);
	
	public static void main(String[] args) throws Exception {
		final DynamoDBInitializer initializer = new DynamoDBInitializer();
		initializer.initialize();
	}
	
	private void initialize() throws Exception {
		final List<String> tableNames = database.listTables().getTableNames();
		if (!tableNames.contains("profile"))
			createTable(database, Profile.class);
		if (!tableNames.contains("content"))
			createTable(database, Content.class);
		
		Thread.sleep(3000);
		
		final DynamoDBScanExpression expression = new DynamoDBScanExpression();
		expression.setSelect(Select.COUNT);
		final int count = mapper.count(Content.class, expression);
		if(0 == count) loadContentData(database);
	}

	private CreateTableResult createTable(AmazonDynamoDB database, Class<?> type) {
		final CreateTableRequest request = mapper.generateCreateTableRequest(type);
		request.setProvisionedThroughput(new ProvisionedThroughput(5L, 5L));
		final CreateTableResult result = database.createTable(request);
		log.info("{}", result.getTableDescription());
		return result;
	}
	
	private static final Set<String> WORDS = Arrays.stream(new String[]{
			"a","an","the","of","for","on","by","to","and","or","not","done","type:","ebook","page:"})
			.collect(Collectors.toSet());
    private void loadContentData(AmazonDynamoDB database) throws JsonParseException, JsonMappingException, IOException {
		final ObjectMapper objectMapper = new ObjectMapper();
		final List<Content> contents = new LinkedList<>();
		final List<Item> items = objectMapper.readValue(path.toFile(), new TypeReference<List<Item>>() {});
		items.forEach(item -> transform(item, contents));
		mapper.batchSave(contents);
    }
    
    private void transform(Item item, List<Content> contents) {
		final Set<String> tags = Arrays.stream(item.getTags().split("[ ,]+"))
				.map(String::trim)
				.map(String::toLowerCase)
				.filter(tag -> !WORDS.contains(tag))
				.collect(Collectors.toSet());
		if(StringUtils.hasText(item.getEbook()) && 
				!"NA".equalsIgnoreCase(item.getEbook().trim().toUpperCase())) {
			final Content content = item.toContent();
			content.setUrl(item.getEbook());
			content.setTags(tags);
			content.setType(ContentType.DOCUMENT);
			contents.add(content);
			System.out.println(content);
		}
		if(StringUtils.hasText(item.getUrl()) && 
				!"NA".equalsIgnoreCase(item.getUrl().trim().toUpperCase())) {
			final Content content = item.toContent();
			content.setUrl(item.getUrl());
			content.setTags(tags);
			content.setType(ContentType.VIDEO);
			contents.add(content);
			System.out.println(content);
		}
	}

}

@Data
class Item {
	@JsonProperty("Board") private String board;
	@JsonProperty("Class") private Integer standard;
	@JsonProperty("Subject") private String subject;
	@JsonProperty("Chapter") private String chapter;
	@JsonProperty("Topic") private String topic;
	@JsonProperty("Subtopic") private String subtopic;
	@JsonProperty("URL") private String url;
	@JsonProperty("E-book") private String ebook;
	@JsonProperty("Notes") private String notes;
	@JsonProperty("Tags") private String tags;
	
	public Content toContent() {
		return Content.builder()
				.board(board)
				.standard(standard)
				.subject(subject)
				.chapter(chapter)
				.topic(topic)
				.subtopic(subtopic)
				.description(notes)
				.build();
	}
}
