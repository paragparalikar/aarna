package com.aarna.content;

public enum ContentType {

	DOCUMENT, IMAGE, AUDIO, VIDEO;
	
}
