package com.aarna.content;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/v1/contents")
public class ContentController {

	@Autowired private ContentService contentService;
	
	@GetMapping
	public Page<Content> findAll(
			@RequestParam(defaultValue = "0") @PositiveOrZero int page,
			@RequestParam(defaultValue = "10") @PositiveOrZero @Max(100) int size){
		final PageRequest pageable = PageRequest.of(page, size, Sort.unsorted());
		return contentService.findAll(pageable);
	}
	
	@GetMapping("/{id}")
	public Content findById(@PathVariable @NotBlank @Size(min = 1, max = 256) final String id) {
		return contentService.findById(id);
	}
	
	@PostMapping
	public Content create(@RequestBody @Valid final Content content) {
		if(StringUtils.hasText(content.getId())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, 
					"Identifier must not be provided for creating a resource");
		}
		return contentService.save(content);
	}
	
	@PutMapping
	public Content update(@RequestBody @Valid final Content content) {
		if(!StringUtils.hasText(content.getId())) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, 
					"Identifier must be provided for updating a resource");
		}
		return contentService.save(content);
	}
	
	@DeleteMapping
	public void deletetById(String id) {
		contentService.deleteById(id);
	}
}
