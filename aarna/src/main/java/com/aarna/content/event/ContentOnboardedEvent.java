package com.aarna.content.event;

import com.aarna.content.Content;

import lombok.Value;

@Value
public class ContentOnboardedEvent {

	private final Content content;
	
}
