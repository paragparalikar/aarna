package com.aarna.content.event;

import com.aarna.content.Content;

import lombok.Value;

@Value
public class ContentOffboardedEvent {

	private final Content content;
	
}
