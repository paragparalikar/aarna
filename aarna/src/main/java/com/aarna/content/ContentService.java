package com.aarna.content;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.aarna.content.event.ContentOffboardedEvent;
import com.aarna.content.event.ContentOnboardedEvent;

@Service
public class ContentService {

	@Autowired private ContentRepository contentRepository;
	@Autowired private ApplicationEventPublisher eventPublisher;
	
	public Iterable<Content> findAll(){
		return contentRepository.findAll();
	}
	
	public Page<Content> findAll(Pageable pageable){
		return contentRepository.findAll(pageable);
	}
	
	public Content findById(String id) {
		return contentRepository.findById(id).orElse(null);
	}
	
	public Content save(Content content) {
		final Content result = contentRepository.save(content);
		eventPublisher.publishEvent(new ContentOnboardedEvent(result));
		return result;
	}
	
	public void deleteById(String id) {
		contentRepository.findById(id).ifPresent(content -> {
			contentRepository.delete(content);
			eventPublisher.publishEvent(new ContentOffboardedEvent(content));
		});
	}

}
