package com.aarna.content;

import java.util.Optional;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.socialsignin.spring.data.dynamodb.repository.EnableScanCount;
import org.springframework.data.repository.PagingAndSortingRepository;

@EnableScan
@EnableScanCount
public interface ContentRepository extends PagingAndSortingRepository<Content, String> {

	Optional<Content> findById(String id);
	
}
